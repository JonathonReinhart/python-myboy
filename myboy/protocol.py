from enum import IntFlag, IntEnum
import logging
import io
import warnings
import zlib

from .binio import BinaryReader, BinaryWriter
from .util import recvall, preview, hexdump, FieldReprMixin
from .gamestate import GameState

BYTEORDER = 'big'

logger = logging.getLogger(__name__)

class ProtoVer(IntEnum):
    MyOldBoy = 4    # My OldBoy! (GameBoy / GameBoy Color)
    MyBoy = 6       # My Boy! (GameBoy Advance)

def len_prefix(data, fieldsize):
    return len(data).to_bytes(fieldsize, BYTEORDER) + data

F_LEN_SIZE = 4

# My OldBoy! (GameBoy / GameBoy Color)
class GBCButtonMap(IntFlag):
    Right   = 0x01
    Left    = 0x02
    Up      = 0x04
    Down    = 0x08
    A       = 0x10
    B       = 0x20
    Select  = 0x40
    Start   = 0x80


# My Boy! (GameBoy Advance)
class GBAButtonMap(IntFlag):
    A               = 1
    B               = 2
    Select          = 4
    Start           = 8
    Right           = 0x10
    Left            = 0x20
    Up              = 0x40
    Down            = 0x80
    RightTrigger    = 0x100
    LeftTrigger     = 0x200


class Packet(FieldReprMixin):

    @classmethod
    def recv(cls, sock, ptype_map):
        def binrd(data):
            return BinaryReader.from_bytes(data, default_byteorder=BYTEORDER)

        # Read packet length first
        data = sock.recv(F_LEN_SIZE)
        if not data:
            raise EOFError("Connection closed")
        br = binrd(data)

        packetlen = br.read_int(F_LEN_SIZE)

        # Read variable-length packet
        data = recvall(sock, packetlen)
        logger.debug(f"Received {len(data)} byte packet:    " + preview(data))
        assert len(data) == packetlen   # lol
        br = binrd(data)

        # Get packet type
        ptype = br.read_u16()

        pcls = ptype_map.get(ptype)
        if not pcls:
            raise NotImplementedError(f"Unexpected packet type: {ptype}")

        pkt = pcls.from_binrd(br)

        # Verify binreader was read exhaustively
        extra = br.read_all()
        if extra:
            warnings.warn(f"{pcls.__name__}.from_binrd() did not fully consume packet data: {len(extra)} bytes remain")

        return pkt

    @classmethod
    def recv_initial(cls, sock):
        return cls.recv(sock, ptype_map_initial)

    @classmethod
    def recv_gbc(cls, sock):
        return cls.recv(sock, ptype_map_gbc)

    @classmethod
    def recv_gba(cls, sock):
        return cls.recv(sock, ptype_map_gba)

    def send(self, sock):
        data = len_prefix(self.to_bytes(), F_LEN_SIZE)
        logger.debug(f"Sending {len(data)} bytes (w/ len):    " + preview(data))
        sock.send(data)

    def to_bytes(self):
        bw = BinaryWriter(io.BytesIO(), default_byteorder=BYTEORDER)
        bw.write_u16(self.ptype)

        self._write_bytes(bw)

        return bw.f.getvalue()


class HandshakePacket(Packet):
    ptype = 1

    def __init__(self, ver):
        self.ver = ver

    @classmethod
    def from_binrd(cls, br):
        return cls(
                ver = br.read_u32(),
                )


    def _write_bytes(self, bw):
        bw.write_u32(self.ver)



class ROMInfoPacket(Packet):
    ptype = 2

    def __init__(self, romhash, romname):
        self.romhash = romhash
        self.romname = romname

    @classmethod
    def from_binrd(cls, br):
        romhash = br.readn(32).decode('ascii')
        namelen = br.read_u16()
        romname = br.readn(namelen).decode('ascii')

        return cls(
                romhash = romhash,
                romname = romname,
            )

    def _write_bytes(self, bw):
        bw.write(self.romhash.encode('ascii'))
        bw.write_len_prefixed(self.romname.encode('ascii'), 2)



class ConfirmPacket(Packet):
    ptype = 3

    @classmethod
    def from_binrd(cls, br):
        return cls()

    def _write_bytes(self, bw):
        # empty
        pass


class InputStatePacket(Packet):
    ptype = 4

    def __init__(self, buttons=0):
        self.buttons = buttons

    @classmethod
    def from_binrd(cls, br):
        return cls(
                buttons = cls.buttonmap(br.read_u32()),
                )

    def _write_bytes(self, bw):
        bw.write_u32(self.buttons)

class GBCInputStatePacket(InputStatePacket):
    buttonmap = GBCButtonMap

class GBAInputStatePacket(InputStatePacket):
    buttonmap = GBAButtonMap


class GameStatePacket(Packet):
    ptype = 6

    def __init__(self, compressed_state):
        self.compressed_state = compressed_state

    @classmethod
    def from_binrd(cls, br):
        statelen = br.read_u32()
        statedata = br.readn(statelen)
        return cls(
                compressed_state = statedata,
                )

    def _write_bytes(self, bw):
        bw.write_len_prefixed(self.compressed_state, 4)

    def __repr__(self):
        return f"GameStatePacket(compressed_state={preview(self.compressed_state)})"

    def get_raw_state_data(self):
        return zlib.decompress(self.compressed_state)

    def get_state(self, cls=GameState):
        assert issubclass(cls, GameState)
        return cls.from_bytes(self.get_raw_state_data())


ptype_map_initial = {
    cls.ptype: cls for cls in (
        HandshakePacket,
    )
}

ptype_map_gbc = {
    cls.ptype: cls for cls in (
        HandshakePacket,
        ROMInfoPacket,
        ConfirmPacket,
        GBCInputStatePacket,
        GameStatePacket,
    )
}

ptype_map_gba = {
    cls.ptype: cls for cls in (
        HandshakePacket,
        ROMInfoPacket,
        ConfirmPacket,
        GBAInputStatePacket,
        GameStatePacket,
    )
}

