from .util import rawfield
from .binio import BinaryReader

class GameState:
    @classmethod
    def from_bytes(cls, data):
        magic = cls._read_common_fields(data)['magic']

        stcls = state_map.get(magic)
        if not stcls:
            raise NotImplementedError(f"Unexpected state file magic: {magic}")

        if stcls.from_bytes.__func__ == GameState.from_bytes.__func__:
            raise NotImplementedError(f"{cls.__name__} did not implement from_bytes()")

        return stcls.from_bytes(data)

    @classmethod
    def _read_common_fields(cls, data):
        br = BinaryReader.from_bytes(data, default_byteorder='big')

        # These fields seem to be common
        magic = br.read(8)
        ver = br.read_u32()
        rom_id = br.read(16)

        return dict(
                magic = magic,
                ver = ver,
                rom_id = rom_id,
                )

    def __init__(self, magic, rom_id, ver):
        if magic != self.MAGIC:
            raise ValueError("Unexpected magic")
        if ver != self.VERSION:
            raise ValueError("Unexpected ver")

        self.rom_id = rom_id
        self.ver = ver

    def __repr__(self):
        return f"{type(self).__name__}(rom_id={self.rom_id!r}, ver={self.ver})"

class GBCState(GameState):
    MAGIC = b'GBCSTATE'
    VERSION = 3

    WRAM_BASE = 0xC000
    WRAM_SIZE = 0x2000
    WRAM_END = WRAM_BASE + WRAM_SIZE


    def __init__(self, wram, **kw):
        super().__init__(**kw)

        if len(wram) != self.WRAM_SIZE:
            raise ValueError("Invalid wram size")
        self.wram = wram


    @classmethod
    def from_bytes(cls, data):
        flds = cls._read_common_fields(data)

        return cls(
                **flds,
                wram = rawfield(data, 0x44, cls.WRAM_SIZE),
                )


    def get_wram_bytes(self, addr, n):
        if addr < self.WRAM_BASE or (addr + n) > self.WRAM_END:
            raise ValueError(f"Requested address range 0x{addr:X} +{n} not in WRAM")

        return rawfield(self.wram, addr - self.WRAM_BASE, n)

    def get_wram_int(self, addr, size):
        return int.from_bytes(self.get_wram_bytes(addr, size), 'big')


class GBAState(GameState):
    MAGIC = b'GBASTATE'
    VERSION = 4


    @classmethod
    def from_bytes(cls, data):
        flds = cls._read_common_fields(data)

        return cls(
                **flds,
                )




state_map = {
    cls.MAGIC : cls for cls in (
        GBCState,
        GBAState,
    )
}

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('statefile', type=argparse.FileType('rb'))
    args = ap.parse_args()


    st = GameState.from_bytes(args.statefile.read())
    print(st)
