from binascii import hexlify
from hexdump import dumpgen as hexdumpgen

def hexdump(data, indent=0):
    print('\n'.join((' '*indent) + line for line in hexdumpgen(data)))


def preview(data, limit=16):
    trunc = len(data) > limit
    data = data[:limit]
    out = hexlify(data, ' ').decode('ascii') # what a stupid API
    if trunc:
        out += '...'
    return out


def recvall(s, length):
    chunks = []
    nrecvd = 0
    while True:
        remain = length - nrecvd
        assert remain >= 0
        if not remain:
            break

        chunk = s.recv(remain)
        if not chunk:
            raise EOFError
        chunks.append(chunk)
        nrecvd += len(chunk)

    return b''.join(chunks)


def rawfield(data, offset, length):
    r = data[offset : offset+length]
    if len(r) != length:
        raise EOFError("Truncated data")
    return r


class FieldReprMixin:
    def __repr__(self):
        s = type(self).__name__
        s += '('
        kvs = (f'{k}={v!r}' for k, v in self.__dict__.items() if not k.startswith('_'))
        s += ', '.join(kvs)
        s += ')'
        return s
