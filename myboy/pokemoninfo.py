import logging
from enum import IntEnum
from pprint import pprint

from .gamestate import GBCState
from .server import add_server_args, Server, Connection
from .util import preview
from .util import FieldReprMixin

logger = logging.getLogger(__name__)


def poke_decode(s):
    def translate_one(c):
        # Returns a charcter
        if 0x80 <= c <= (0x80 + 26):
            return chr(c - 0x80 + ord('A'))
        if 0xA0 <= c <= (0xA0 + 26):
            return chr(c - 0xA0 + ord('a'))
        if c == 0x50:
            return None
        print(f"Unknown char 0x{c:X}")
        return '?'  # TODO

    res = ''
    for x in s:
        c = translate_one(x)
        if not c:
            break
        res += c

    return res

# https://github.com/pret/pokegold/blob/master/constants/map_constants.asm
MAP_GROUP_NAMES = {
    1:  "OLIVINE",
    2:  "MAHOGANY",
    3:  "DUNGEONS",
    4:  "ECRUTEAK",
    5:  "BLACKTHORN",
    6:  "CINNABAR",
    7:  "CERULEAN",
    8:  "AZALEA",
    9:  "LAKE_OF_RAGE",
    10: "VIOLET",
    11: "GOLDENROD",
    12: "VERMILION",
    13: "PALLET",
    14: "PEWTER",
    15: "FAST_SHIP",
    16: "INDIGO",
    17: "FUCHSIA",
    18: "LAVENDER",
    19: "SILVER",
    20: "CABLE_CLUB",
    21: "CELADON",
    22: "CIANWOOD",
    23: "VIRIDIAN",
    24: "NEW_BARK",
    25: "SAFFRON",
    26: "CHERRYGROVE",
}

# Double-indirect: [map_group][map_number]
# TODO: For now, only contains Johoto routes
# (really only care about data/wild/roammon_maps.asm)
MAP_NUMBERS = {
    1: {    # OLIVINE
        12: "ROUTE_38",
        13: "ROUTE_39",
    },
    2: {    # MAHOGANY
        5:  "ROUTE_42",
        6:  "ROUTE_44",
    },
    5:  {   # BLACKTHORN
        8:  "ROUTE_45",
        9:  "ROUTE_46",
    },
    8:  {   # AZALEA
        6:  "ROUTE_33",
    },
    9:  {   # LAKE_OF_RAGE
        5:  "ROUTE_43",
    },
    10: {   # VIOLET
        1:  "ROUTE_32",
        2:  "ROUTE_35",
        3:  "ROUTE_36",
        4:  "ROUTE_37",
    },
    11: {   # GOLDENROD
        1:  "ROUTE_34",
    },
    19: {   # SILVER
        1:  "ROUTE_28",
    },
    22: {   # CIANWOOD
        1:  "ROUTE_40",
        2:  "ROUTE_41",
    },
    24: {   # NEW_BARK
        1:  "ROUTE_26",
        2:  "ROUTE_27",
        3:  "ROUTE_29",
    },
    26: {   # CHERRYGROVE
        1:  "ROUTE_30",
        2:  "ROUTE_31",
    },
}

def describe_map(mapgrp, mapnum):
    grp_name = MAP_GROUP_NAMES.get(mapgrp, "<<unknown map group>>")

    map_name = "<<unknown map group>>"
    grp_maps = MAP_NUMBERS.get(mapgrp)
    if grp_maps:
        map_name = grp_maps.get(mapnum, "<<unknown map number>>")

    return grp_name + ": " + map_name

class Pokemon(IntEnum):
    RAIKOU      = 0xf3
    ENTEI       = 0xf4
    SUICUNE     = 0xf5

    @classmethod
    def value_map(cls):
        return {p.value: p.name for p in Pokemon}

    @classmethod
    def get_by_id(cls, id_, default=None):
        return cls.value_map().get(id_, default)

class RoamStruct(FieldReprMixin):
    def __init__(self, **kw):
        self.__dict__.update(kw)

    @classmethod
    def from_wram(cls, state, addr):
        return cls(
            species     = state.get_wram_int(addr+0, 1),
            level       = state.get_wram_int(addr+1, 1),
            map_group   = state.get_wram_int(addr+2, 1),
            map_number  = state.get_wram_int(addr+3, 1),
            hp          = state.get_wram_int(addr+4, 1),
            DVs         = state.get_wram_int(addr+5, 2),
        )

    def describe_map(self):
        if self.map_group == 255:
            return "<<invalid>>"
        return describe_map(self.map_group, self.map_number)

    def print(self):
        specname = Pokemon.get_by_id(self.species, f"<Unknown ({self.species})>")
        print(f"  Species:      {specname}")
        print(f"  Level:        {self.level}")
        print(f"  HP:           {self.hp}")
        print(f"  DVs:          {self.DVs}")
        print(f"  Location:     " + self.describe_map())


class PokemonGoldSilverState(GBCState):
    # As far as WRAM is concerned, Gold and Silver are identical.
    # https://raw.githubusercontent.com/pokemon-speedrunning/symfiles/master/pokegold.sym
    # https://raw.githubusercontent.com/pokemon-speedrunning/symfiles/master/pokesilver.sym

    def __init__(self, **kw):
        rom_id = kw['rom_id'][:15]
        if not rom_id in (b'POKEMON_SLVAAXE', b'POKEMON_GLDAAUE'):
            raise ValueError(f"Invalid ROM ID: {rom_id}")
        super().__init__(**kw)

    @property
    def money(self):
        return self.get_wram_int(0xD573, 3)

    @property
    def player_id(self):
        return self.get_wram_int(0xD1A1, 2)

    @property
    def player_name(self):
        return poke_decode(self.get_wram_bytes(0xD1A3, 10))

    @property
    def rival_name(self):
        return poke_decode(self.get_wram_bytes(0xD1B9, 10))

    @property
    def party_pkmn1_name(self):
        return poke_decode(self.get_wram_bytes(0xDB8C, 10))

    ########################################
    # Current location
    # https://github.com/pret/pokegold/blob/master/constants/map_constants.asm

    @property
    def wMapGroup(self):
        return self.get_wram_int(0xDA00, 1)

    @property
    def wMapNumber(self):
        return self.get_wram_int(0xDA01, 1)

    @property
    def wYCoord(self):
        return self.get_wram_int(0xDA02, 1)

    @property
    def wXCoord(self):
        return self.get_wram_int(0xDA03, 1)

    ########################################
    # Roaming Pokemon

    def __get_roam_struct(self, addr):
        return RoamStruct.from_wram(self, addr)

    @property
    def wRoamMon1(self):
        return self.__get_roam_struct(0xDD1A)

    @property
    def wRoamMon2(self):
        return self.__get_roam_struct(0xDD21)

    @property
    def wRoamMon3(self):
        return self.__get_roam_struct(0xDD28)

    @property
    def wRoamMons(self):
        return [
                self.wRoamMon1,
                self.wRoamMon2,
                self.wRoamMon3,
            ]

    @property
    def wRoamMons_CurMapNumber(self):
        return self.get_wram_int(0xDD2F, 1)

    @property
    def wRoamMons_CurMapGroup(self):
        return self.get_wram_int(0xDD30, 1)

    @property
    def wRoamMons_LastMapNumber(self):
        return self.get_wram_int(0xDD31, 1)

    @property
    def wRoamMons_LastMapGroup(self):
        return self.get_wram_int(0xDD32, 1)

    def print(self):
        print("-"*80)
        print(f"Player name:        {self.player_name!r}")
        print(f"Player ID:          {self.player_id}")
        print(f"Money:              ₽{self.money}")
        print(f"Rival name:         {self.rival_name!r}")
        print()
        print(f"Current location:   " + describe_map(self.wMapGroup, self.wMapNumber) + f"  X={self.wXCoord}, Y={self.wYCoord}")

        roam_mons = self.wRoamMons
        for i in range(0, 3):
            m = roam_mons[i]

            in_same_map = (m.map_group == self.wMapGroup and m.map_number == self.wMapNumber)
            msg = "\033[32m      *** SAME MAP! ***\033[m" if in_same_map else ""

            print(f"\nRoaming Pkmn {i+1}:" + msg)
            m.print()





class DumpStateConnection(Connection):
    def handle_game_state(self, packet):
        st = packet.get_state(PokemonGoldSilverState)
        logger.info("Got state!")
        st.print()

        return False


class DumpStateServer(Server):
    def create_connection(self, sock):
        return DumpStateConnection(sock=sock)


def parse_args():
    import argparse
    ap = argparse.ArgumentParser()
    add_server_args(ap)
    return ap.parse_args()

def main():
    #logging.basicConfig(level=logging.DEBUG)

    args = parse_args()
    s = DumpStateServer.from_args(args)

    with s:
        s.run()


if __name__ == '__main__':
    main()
