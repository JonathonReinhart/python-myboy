import socket
import logging

from .protocol import (
        ProtoVer,
        Packet,
        HandshakePacket,
        ROMInfoPacket,
        ConfirmPacket,
        InputStatePacket,
        GameStatePacket,
    )

logger = logging.getLogger(__name__)

DEFAULT_PORT = 6374

class Connection:
    def __init__(self, sock):
        self.sock = sock
        self.recv_packet = Packet.recv_initial

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.close()

    def close(self):
        self.sock.close()
        self.sock = None

    def run(self):
        while True:
            packet = self.recv_packet(self.sock)
            logger.debug(f"Got packet: {packet}")
            if self.handle_packet(packet) == False:
                logger.info("Handler requested stop")
                break

    def send(self, packet):
        assert isinstance(packet, Packet)
        packet.send(self.sock)

    def handle_packet(self, packet):
        handlers = (
            (HandshakePacket,    self.handle_handshake),
            (ROMInfoPacket,      self.handle_rominfo),
            (ConfirmPacket,      self.handle_confirm),
            (InputStatePacket,   self.handle_input_state),
            (GameStatePacket,    self.handle_game_state),
        )

        for pktcls, handler in handlers:
            if isinstance(packet, pktcls):
                return handler(packet)

        raise NotImplementedError(f"No handler for {type(packet)}")

    def handle_handshake(self, packet):
        logger.debug(f"Got handshake: {packet}")
        if packet.ver == ProtoVer.MyOldBoy:
            logger.info("Connection from My OldBoy! (GameBoy Color)")
            self.recv_packet = Packet.recv_gbc

        elif packet.ver == ProtoVer.MyBoy:
            logger.info("Connection from My Boy! (GameBoy Advance)")
            self.recv_packet = Packet.recv_gba

        self.send(packet) # Reflect

    def handle_rominfo(self, packet):
        self.send(packet) # Reflect

    def handle_confirm(self, packet):
        self.send(packet) # Reflect

    def handle_input_state(self, packet):
        reflect = True
        if reflect:
            self.send(packet)   # Reflect
        else:
            self.send(InputStatePacket())   # Empty

    def handle_game_state(self, packet):
        st = packet.get_state()
        logger.debug("Got state: " + str(st))
        self.send(packet) # Reflect




class Server:
    def __init__(self, addr='0.0.0.0', port=DEFAULT_PORT):
        listen_addr = (addr, port)

        self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.lsock.bind(listen_addr)
        self.lsock.listen(1)
        logger.info(f"Listening on {listen_addr}")

    @classmethod
    def from_args(cls, args):
        return cls(addr=args.listenaddr, port=args.listenport)

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.close()

    def close(self):
        self.lsock.close()
        self.lsock = None

    def create_connection(self, sock):
        return Connection(sock=sock)

    def run(self):
        while True:
            logger.info("Waiting for connection...")
            csock, client_addr = self.lsock.accept()
            logger.info(f"Accepted connection from {client_addr}")

            with self.create_connection(sock=csock) as conn:
                conn.run()


def add_server_args(ap):
    ap.add_argument('-a', '--addr', dest='listenaddr', default='0.0.0.0',
            help='Listen IP address')
    ap.add_argument('-p', '--port', dest='listenport', type=int, default=DEFAULT_PORT,
            help='Listen port')


def parse_args(args=None):
    import argparse
    ap = argparse.ArgumentParser()
    add_server_args(ap)
    return ap.parse_args(args=args)


def main():
    logging.basicConfig(level=logging.DEBUG)

    args = parse_args()
    with Server.from_args(args) as s:
        s.run()


if __name__ == '__main__':
    main()
