import io

class BinaryReader:
    def __init__(self, f, default_byteorder=None):
        if not isinstance(f.read(0), bytes):
            raise ValueError("File not open in binary mode")
        self.f = f
        self.default_byteorder = default_byteorder

    @classmethod
    def from_bytes(cls, data, **kw):
        return cls(io.BytesIO(data), **kw)

    def read(self, size=-1):
        return self.f.read(size)

    def readn(self, size):
        """Read exactly size bytes"""
        result = b""
        remain = size

        while remain:
            b = self.read(remain)
            if not b:
                raise EOFError()
            result += b
            assert len(b) <= remain
            remain -= len(b)

        assert len(result) == size
        return result

    def read_all(self):
        """Read all remaining data"""
        result = b""

        while True:
            b = self.read()
            if not b:
                break
            result += b

        return result

    def read_int(self, size, byteorder=None, signed=False):
        byteorder = byteorder or self.default_byteorder
        if not byteorder:
            raise ValueError("byteorder not provided and default_byteorder not set")

        return int.from_bytes(self.readn(size), byteorder, signed=signed)

    def read_i8(self, byteorder=None):
        return self.read_int(1, byteorder, True)

    def read_u8(self, byteorder=None):
        return self.read_int(1, byteorder)

    def read_i16(self, byteorder=None):
        return self.read_int(2, byteorder, True)

    def read_u16(self, byteorder=None):
        return self.read_int(2, byteorder)

    def read_i32(self, byteorder=None):
        return self.read_int(4, byteorder, True)

    def read_u32(self, byteorder=None):
        return self.read_int(4, byteorder)

    def read_i64(self, byteorder=None):
        return self.read_int(8, byteorder, True)

    def read_u64(self, byteorder=None):
        return self.read_int(8, byteorder)


class BinaryWriter:
    def __init__(self, f, default_byteorder=None):
        # TODO: assert f is binary mode file
        self.f = f
        self.default_byteorder = default_byteorder

    def write(self, data):
        written = self.f.write(data)
        assert written == len(data)     # TODO: use a write-all loop
        return written

    def write_int(self, value, size, byteorder=None, signed=False):
        byteorder = byteorder or self.default_byteorder
        if not byteorder:
            raise ValueError("byteorder not provided and default_byteorder not set")

        data = value.to_bytes(size, byteorder, signed=signed)
        self.write(data)

    def write_i8(self, value, byteorder=None):
        return self.write_int(value, 1, byteorder, True)

    def write_u8(self, value, byteorder=None):
        return self.write_int(value, 1, byteorder)

    def write_i16(self, value, byteorder=None):
        return self.write_int(value, 2, byteorder, True)

    def write_u16(self, value, byteorder=None):
        return self.write_int(value, 2, byteorder)

    def write_i32(self, value, byteorder=None):
        return self.write_int(value, 4, byteorder, True)

    def write_u32(self, value, byteorder=None):
        return self.write_int(value, 4, byteorder)

    def write_i64(self, value, byteorder=None):
        return self.write_int(value, 8, byteorder, True)

    def write_u64(self, value, byteorder=None):
        return self.write_int(value, 8, byteorder)

    def write_len_prefixed(self, data, fieldsize, byteorder=None):
        self.write_int(len(data), fieldsize, byteorder)
        self.write(data)
