import logging

from .server import add_server_args, Server, Connection
from .util import preview

logger = logging.getLogger(__name__)


class DumpStateConnection(Connection):
    def __init__(self, statefile, **kw):
        super().__init__(**kw)
        self.statefile = statefile

    def handle_game_state(self, packet):
        st = packet.get_state()
        logger.info(f"Got state object: {st}")

        stdata = packet.get_raw_state_data()
        logger.info("Got state!  " + preview(stdata))

        self.statefile.write(stdata)
        logger.info(f"Wrote state to {self.statefile}")
        return False


class DumpStateServer(Server):
    def __init__(self, statefile, **kw):
        super().__init__(**kw)
        self.statefile = statefile

    @classmethod
    def from_args(cls, args):
        # TODO: How to DRY with base class from_args()?
        return cls(addr=args.listenaddr, port=args.listenport, statefile=args.statefile)

    def close(self):
        super().close()
        self.statefile.close()

    def create_connection(self, sock):
        return DumpStateConnection(sock=sock, statefile=self.statefile)


def parse_args():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('statefile', type=argparse.FileType('wb'))
    add_server_args(ap)
    return ap.parse_args()

def main():
    logging.basicConfig(level=logging.DEBUG)

    args = parse_args()
    s = DumpStateServer.from_args(args)

    with s:
        s.run()


if __name__ == '__main__':
    main()
