python-myboy
============
This is a research project relating to the "My Boy!" and "My OldBoy!" Gameboy
emulators for Android.

These apps support linking two emulated Gameboys (as with a link cable on real
hardware). This project focuses on that protocol behind the "Link Remote"
feature. The TCP protocol is described in `doc/protocol.txt`.

This Python package provides an implementation of the protocol:

- `myboy`
  - `binio` -- Binary reader/writer classes.
  - `dumpstate` -- A server that will dump game state to file.
  - `gamestate` -- Handles the emulator game state files. 
  - `pokemoninfo` -- A server that displays interesting information from
    Pokemon Gold / Silver game state
  - `protocol` -- The per-packet handling of the link protocol
  - `server` -- A server which simply reflects all messages
  - `util` -- Misc util functions

# Use
Run one of the three server tools:
- `python3 -m myboy.server`
- `python3 -m myboy.dumpstate output.state`
- `python3 -m myboy.pokemoninfo`

Then connect to your computer from the emulator:  
- `☰` > `Link remote` > `Wi-Fi (client)`
- IP address: Your computer's IP
- Port: (leave set to default, 6374)


# Background
I was playing Pokemon Silver on My OldBoy and decided to make some trades with
Pokemon Gold running on another device (I hadn't gotten the hang of switching
between instances with "Link Local"). I became curious about what the protocol
looked like, and what one might be able to do by spoofing it.

When I followed the stream in Wireshark, I was surprised to see nothing that
looked like an emulated serial connection. Instead, there was a big handshake,
followed by some minimal steady-state traffic.

It turns out that the emulator take an interesting approach:
- Ensure both sides of the link have the opposing party's ROM (by MD5 hash)
- Exchange the full game state between parties
- Emulate the opposing party's game in a background instance
- Exchange input (button) state in lockstep
- Handle the serial "link" locally.

With this approach, I wouldn't be able to modify the data being exchanged on
the serial link. Howver, it *would* provide a mechanism for viewing the game
state at any time.

At this point, I had caught/traded 248 of the 250 Pokemon in the game,
including one of the three legendary
[Roaming Pokemon](https://bulbapedia.bulbagarden.net/wiki/Roaming_Pok%C3%A9mon#Generation_II),
having encountered it by sheer luck. The problem is that, until you encounter
the Pokemon in the wild, its location is not visible in your Pokedex.

Enter `myboy.pokemoninfo`. The primary goal of this tool is to display the
location of the three roaming pokemon, even though they have not yet been
encountered.
